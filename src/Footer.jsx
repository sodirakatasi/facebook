import React from 'react';

export default function Footer() {
  return (
    <>
      <div className="">
                <div className="_95ke _8opy">
                    <div id="pageFooter" data-referrer="page_footer" data-testid="page_footer">
                        <ul className="uiList localeSelectorList _2pid _509- _4ki _6-h _6-j _6-i" data-nocookies="1">
                            <li>Bahasa Indonesia</li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://en-gb.facebook.com/" title="English (UK)">English (UK)</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://jv-id.facebook.com/"  title="Javanese">Basa Jawa</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://ms-my.facebook.com/"  title="Malay">Bahasa Melayu</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://ja-jp.facebook.com/"  title="Japanese">日本語</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="rtl" href="https://ar-ar.facebook.com/"  title="Arabic">العربية</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://fr-fr.facebook.com/"  title="French (France)">Français (France)</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://es-la.facebook.com/"  title="Spanish">Español</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://ko-kr.facebook.com/"  title="Korean">한국어</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://pt-br.facebook.com/"  title="Portuguese (Brazil)">Português (Brasil)</a>
                            </li>
                            <li>
                              <a className="_sv4" dir="ltr" href="https://de-de.facebook.com/"  title="German">Deutsch</a>
                            </li>
                            <li>
                              <a role="button" className="_42ft _4jy0 _517i _517h _51sy" rel="dialog" ajaxify="/settings/language/language/?uri=https%3A%2F%2Fde-de.facebook.com%2F&amp;source=www_list_selector_more" href="#" title="Tampilkan bahasa lainnya">
                              <i className="img sp_ot1t5YjYL3s sx_2cfa7d">
                              </i>
                            </a>
                            </li>
                        </ul>
                        <div id="contentCurve">

                        </div>
                        <div id="pageFooterChildren" role="contentinfo" aria-label="Tautan situs Facebook">
                            <ul className="uiList pageFooterLinkList _509- _4ki _703 _6-i">
                                <li>
                                  <a href="/reg/" title="Daftar Facebook">Daftar</a>
                                </li>
                                <li>
                                  <a href="/login/" title="Masuk ke Facebook">Masuk</a>
                                </li>
                                <li>
                                  <a href="https://messenger.com/" title="Coba Messenger.">Messenger</a>
                                </li>
                                <li>
                                  <a href="/lite/" title="Facebook Lite untuk Android.">Facebook Lite</a>
                                </li>
                                <li>
                                  <a href="https://www.facebook.com/watch/" title="Telusuri video Watch kami.">Watch</a>
                                </li>
                                <li>
                                  <a href="/places/" title="Periksa tempat-tempat populer di Facebook.">Tempat</a>
                                </li>
                                <li>
                                  <a href="/games/" title="Periksa game Facebook.">Game</a>
                                </li>
                                <li>
                                  <a href="/marketplace/" title="Beli dan jual di Facebook Marketplace.">Marketplace</a>
                                </li>
                                <li>
                                  <a href="https://pay.facebook.com/" title="Pelajari selengkapnya tentang Facebook Pay" target="_blank">Facebook Pay</a>
                                </li>
                                <li>
                                  <a href="https://www.oculus.com/" title="Pelajari Selengkapnya Tentang Oculus" target="_blank">Oculus</a>
                                </li>
                                <li>
                                  <a href="https://portal.facebook.com/" title="Pelajari selengkapnya tentang Facebook Portal" target="_blank">Portal</a>
                                </li>
                                <li>
                                  <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.instagram.com%2F&amp;h=AT0cvIquKD3WPsR1_Nf90qyonQPp3XFsj6yV0TK1hh5z2GW8C0RR67AzZMfLu7I4z0s2gHHPTFV0jGf0f4xibXrY-lOI9gdU1LWnJp78mOw9RsE2gdJW6sL-wJaiNVr9ZTa3IWyYpj7v6voKcfxp8egOLxo" title="Coba Instagram" target="_blank" rel="noopener nofollow" data-lynx-mode="asynclazy">Instagram</a>
                                </li>
                                <li>
                                  <a href="https://www.bulletin.com/" title="Lihat Buletin dari Bulletin">Bulletin</a>
                                </li>
                                <li>
                                  <a href="/local/lists/245019872666104/" title="Jelajahi direktori Daftar Lokal kami.">Lokal</a>
                                </li>
                                <li>
                                  <a href="/fundraisers/" title="Berdonasi ke gerakan yang bermanfaat.">Penggalangan Dana</a>
                                </li>
                                <li>
                                  <a href="/biz/directory/" title="Jelajahi direktori Layanan Facebook kami.">Layanan</a>
                                </li>
                                <li>
                                  <a href="/votinginformationcenter/?entry_point=c2l0ZQ%3D%3D" title="Lihat Pusat Informasi Pemilu">Pusat Informasi Pemilu</a>
                                </li>
                                <li>
                                  <a href="/groups/explore/" title="Jelajahi Grup kami.">Grup</a>
                                </li>
                                <li>
                                  <a href="https://about.facebook.com/" accessKey="8" title="Baca blog kami, temukan pusat sumber daya, dan cari peluang kerja.">Tentang</a>
                                </li>
                                <li>
                                  <a href="/ad_campaign/landing.php?placement=pflo&amp;campaign_id=402047449186&amp;nav_source=unknown&amp;extra_1=auto" title="Beriklan di Facebook.">Buat Iklan</a>
                                </li>
                                <li>
                                  <a href="/pages/create/?ref_type=site_footer" title="Buat halaman">Buat Halaman</a>
                                </li>
                                <li>
                                  <a href="https://developers.facebook.com/?ref=pf" title="Buat aplikasi di platform kami.">Developer</a>
                                </li>
                                <li>
                                  <a href="/careers/?ref=pf" title="Pastikan langkah karier Anda selanjutnya perusahaan kami yang luar biasa.">Karier</a>
                                </li>
                                <li>
                                  <a data-nocookies="1" href="/privacy/policy/?entry_point=facebook_page_footer" title="Bacalah tentang privasi Anda dan Facebook.">Privasi</a>
                                </li>
                                <li>
                                  <a href="/policies/cookies/" title="Pelajari tentang cookie dan Facebook." data-nocookies="1">Cookie</a>
                                </li>
                                <li>
                                  <a className="_41ug" data-nocookies="1" href="https://www.facebook.com/help/568137493302217" title="Pelajari tentang Pilihan Iklan.">Pilihan Iklan<i className="img sp_ot1t5YjYL3s sx_708a0f">
                                  </i>
                                </a>
                                </li>
                                <li>
                                  <a data-nocookies="1" href="/policies?ref=pf" accessKey="9" title="Tinjau ketentuan dan kebijakan kami.">Ketentuan</a>
                                </li>
                                <li>
                                  <a href="/help/?ref=pf" accessKey="0" title="Kunjungi Pusat Bantuan kami.">Bantuan</a>
                                </li>
                                <li>
                                  <a href="help/637205020878504" title="Kunjungi Pemberitahuan Pengunggahan Kontak &amp; Non-Pengguna kami.">Pengunggahan Kontak &amp; Non-Pengguna</a>
                                </li>
                                <li>
                                  <a accessKey="6" className="accessible_elem" href="/settings" title="Lihat dan edit pengaturan Facebook Anda.">Pengaturan</a>
                                </li>
                                <li>
                                  <a accessKey="7" className="accessible_elem" href="/allactivity?privacy_source=activity_log_top_menu" title="Lihat log aktivitas Anda">Log aktivitas</a>
                                </li>
                            </ul>
                        </div>
                        <div className="mvl copyright">
                            <div>
                              <span> Meta © 2022</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </>
  );
}
