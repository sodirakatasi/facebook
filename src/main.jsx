import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './style1.css'
import './style2.css'
import './style3.css'
import './style4.css'

ReactDOM.createRoot(document.getElementById('u_0_1_6R')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)
